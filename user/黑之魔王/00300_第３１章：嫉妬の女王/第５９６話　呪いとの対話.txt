回想起來，我也只有來看房子時，走過一次這從通向地下的樓梯。我現在，位於在這個房子中隱藏的地下二層，面對著菲奧娜的魔女工房。


擋住在我面前的是一面稍微有點生銹的鐵門。當然，它是被鎖上了的。如果這只是一扇普通的鐵門的話，那我的確能靠蠻力衝進去，但菲奧娜對這門已經附加了多層強化。對於魔女來說，工房是她們探索魔道極致的研究室，亦是製造出各種各樣的魔法藥水，魔道具的工廠，同時還是收藏自己創造出的魔法和許多貴重素材的倉庫。為了守護魔法的秘密和自己的財產，自然會是嚴防死守。


因為是菲奧娜，要是想強行破門而入的話，就算觸發了炸掉整個房子的爆破陷阱也不奇怪。

但我沒有能解除天才魔女佈置的難解陷阱的能力。想要解除掉這陷阱，可需要熟悉陷阱的盜賊和精通魔法的魔術士這兩份的知識和能力。


因為我不打算去侵犯她的隱私，所以即使我不能打開這門也沒問題，但是只有這次，我必須要使用這裡。

“嗯……解除的方法是……”

並且，菲奧娜像預先知道了我會使用她的工房一樣，讓薇薇安告訴了我開門的方法。

方法很簡單。把手放在門上，然後念出咒語。

“菲奧娜，我愛你”

金屬摩擦。發出沉重的咯吱聲，鎖開了。同時，我還沒推門，門就自動打開了，在完全沒有光亮的昏暗地下室裡，一朵赤紅的火焰，在燈中燃燒了起來。

真厲害啊，門和照明都是全自動的。

菲奧娜的魔法都比得上現代的科技了，我一邊如此佩服著，一邊走進了魔女的工房。

“裡面很普通啊”

房間裡十分整潔有序，似乎她沒做任何壞事一般。排列好的大架子上，分別按素材、工具、魔道書等種類劃分好了。房間的角落裡還擺放著種有五顏六色的小花和仙人掌之類的植物的花盆。或許是因為她抓了薇薇安，所以從天花板上還吊了個空空的鳥籠下來。


並且，最有魔女風格的東西，就是她放在房間裡的，這口巨大的鍋吧。在這樣的密室裡，要是這個都能把我裝進去的大鍋燒起來了，那該怎麼辦啊？不過也不用杞人憂天，菲奧娜肯定能用魔法解決這種情況的


我總覺得這裡已經是個成熟的工房了,不過，雖說如此，我卻不感覺這裡會帶來災禍，或是十分的可怕。也不是特別血腥。

來看房子的時候，我用黑化，封印了怨念的聲音,不過，這本就是詛咒宅邸，在這凝聚著最深重的怨念的地下室，普通人都會覺得十分陰冷。


但是，現在卻完全沒有任何感覺。十分平靜，完全感覺不到不安的詛咒的跡象。這反而讓人毛骨悚然。仿彿，連詛咒的怨念也害怕著房主，拼死收斂著氣息。看來菲奧娜不僅靠我的黑化，還用上了別的什麼手段，將這個工房打理成自己滿意的環境了啊。

“好，果然在這裡的話，應該就沒問題了。”

我之後要做的修行，就是和詛咒武器對話。


這是我在使用第四加護愛之魔王，與寄宿在暴君之鎧中的米麗雅對話後，我才發覺可以直接和詛咒溝通。如果我能更深刻地理解詛咒的本質，那應該也能獲得更多的力量。

但是，這自然是十分危險的事情。本來，人只要接觸到詛咒就會發狂。實際上，我也見到了因詛咒瘋狂的傢伙。無論我有多高的耐性，我的身體多麼適應詛咒，都有著界限。

直接去做的話還是太危險了。『暴君之鎧』那時候，應該說是運氣好吧。那只是因為米麗雅希望自己能被使用，她才接受了我。


詛咒有各種各樣的類型。怨恨著什麼，或是詛咒著什麼，或是，甚至都存在沒發現自己是詛咒的情況。所以，如果是像小柩和米麗雅這樣，希望有人使用的類型的話，還有溝通的餘地,不過，若是憎恨著整個世界，想要毀滅一切的詛咒，那就沒法交流了，如果是只有毀滅意志的詛咒，就會拒絕我，並侵蝕我的精神吧。要是遇到這種情況，『愛之魔王』又能防到什麼地步呢？即使我不至於發狂，但考慮到會在精神上留下後遺症的風險，直接與詛咒交談還是太危險了。


但是，現在即使多少有點風險，也必須要增強力量。

我的對手是莉莉。而且，她擁有著比我所認識的她要更強的能力。恐怕，現在的莉莉可以與使徒匹敵。

對上這樣的她，不是殺掉，而是必須活捉她。以我現在的力量，還是不大可能。

“要開始了嗎？”

莉莉和菲奧娜，兩人我都要。所以我可不能只抱有輕率的努力和覺悟，這是我對自己的考驗。如果不能跨越的話，就沒有愛她們的資格。

“來吧，『絕怨鉈「首斷」』”

在燈光照耀下的我的影子中，突然無聲無息地出現了巨大的柴刀。真紅的線條象血管一樣爬滿漆黑的刀身，無比不祥的詛咒之刃。其銘為『絕怨鉈「首斷」』。她是我得到的第一把詛咒武器，也是陪伴我最長時間，最為可靠的伙伴。


那麼，我倆該從何談起呢？

我這麼想著，卻不知該如何開口，十分苦惱。

“呼，什麼話都行吧，我對你可沒絲毫隱瞞——愛的魔王”

於是，我為了觸碰到她的心，像往常一樣，握住她的刀柄。


與其說吸附到手中，不如說是如臂使指般的感覺。她十分輕盈。似乎我的神經都長到了刀尖一般。

作為刀來說，這是十分理想的感覺，但是還不夠。僅僅如此，是打不過莉莉的。


我想要力量。我想要更多的，你的力量。

“要怎麼做，你才能變得更強”

請告訴我。

這份感情，通過我握著的刀柄傳達了過去。


她是否會回應我，取決於‘首斷’自身——突然，在我脊背發寒的同時，我的手臂也顫抖了起來。

“哇啊！？”

與顫抖一起傳來的，是詛咒的意志。殺，死，斬，混雜這種強烈的殺意和惡意，混沌又純粹的瘋狂。這是我第一次握著刀刃時，感覺到的意志。


平時我都用黑化壓抑著這意志。如果是普通人的話，這意志可是他們一接觸就會發瘋的危險物，特別是在『首斷』經過了兩次進化後，她的怨念已經濃重到最初的『辻斬』無法比擬的地步了。就算是我，如果整天都聽著這詛咒之聲，也會出問題吧。


正因為如此，我才會用黑化抑制她。基本上，詛咒武器的使用方法，就是看你能忍受詛咒的侵蝕到什麼程度。我的黑化，就是十分出色的忍耐方法。

但是，這樣不行。單方面的壓制、支配、擺弄她，是無法發揮出她真正的力量的。

若不將手伸向這被瘋狂的詛咒之刀的鋒刃處的話，那就無法明白『首斷』的真心。

“——恩，終於平息了嗎？”

我停下了顫抖。看來我比自己預料的要更能忍耐精神上的衝擊吧。我注意到我除了感到疲勞之外，全身都被汗水給浸濕了。


但是，這並不是結束。從現在開始，才是正戲。

去感知吧。我現在正試圖接觸‘首斷’的意志。


手中握著的刀刃突然急劇變重。是如外表般，不，是像是比外表還要大的鐵塊似的，可怕的沉重感。

黑色的刀刃上散發著紅色的光芒。即使外觀沒有改變，我也清楚。現在，從第一次握住她的時候開始，我一直使用的黑化，已經完全解除了。

“來，回應我吧。”

怎樣做才能變得更強？我才能更熟練的使用你。

“……血”

我聽見了，微弱的女性的聲音。

和去斯特拉托斯鐵匠鋪接她的時候聽到的聲音一樣。


到底，她是對故事中那為愛痴狂的村姑，還是像小柩一樣，是新生的詛咒之子呢？

哪個都行。不管你是什麼人，無論你是多麼邪惡的存在，你都是我的伙伴。

“血……給我……你的、血、給我”

腦海中回響的聲音，確實是詛咒般的台詞。她要我獻上我的血。

“什麼呀，這就行了嗎？”

真是個可愛的願望啊。不是要命而是要血。

“這麼說來，你的確吃過怪物的，十字軍士兵的，還有菲奧娜的血，但是沒有吃過我的血啊。”

或許，作為使用者的我，一開始就應該為她獻上鮮血。

“對不起，我沒注意到這事。既然你想要我的血，那我就給你喝。想喝多少喝多少”

我毫不猶豫地將刀刃砍到左手的手腕上，並划了下去。

鮮血噴涌——本應該是這樣的,不過，砍進手腕裡的刀刃，像吸附上般緊貼著傷口，一滴血都沒流出來。原來如此，確實，是吸著啊。我從被切斷的手腕的血管處，直接感受到刀刃在吸收著血液。

咚，咚，隨著脈搏，刀身吸走了我的鮮血。

“……嗚”

突然間，我醒了過來。回過神來，我倒在了冰冷的地板上。

變成這個狀況。看來是我完全沒注意到，就昏過去了。

“頭好暈……看來，吸走了相當多的血啊”

她大概吸走了能把普通人吸乾的血吧。但我沒啥事，只需要造出失去的分量的血就行。


我的治癒魔法『肉體填補』，只是用像是肉體的，果凍狀的魔力物質來填補傷口,不過，在得到了第六加護的現在，這個『肉體填補』就能製造出更加接近真體的東西了。這與其說是海之魔王能力，不如說是利用了模擬水屬性的屬性。


如果熟練的話，就連肉體本身也可以再生出來……而現在，我正在全力製造血液。

但只要有魔力，自己就能恢復，本來除了輸血以外，沒有補充辦法的『血液』，這事還是有著極大的意義。即使受了傷，但只要有魔力就不用擔心失血問題，所以對於沒有補給的長期戰來說，也是十分有利的。


首先，從現在我還沒事的狀況就能看出，用魔法製作出的血液，不是像生理鹽水一樣，單純的替用品，而是好好地再現出了以紅血球為首的血細胞成分。雖說這是用命得出的結論。


總之，我有了能製造出血液的治癒魔法，是啊，也給它起個單純的造血之名吧。

正因為有了這個，所以才能讓『首斷』隨意地吸血。

“那麼，心情如何，大小姐”

『絕怨鉈』隨著我站起身來，也浮在了我的影子上。

她沒有回話。是因為我沒有握住刀柄嗎？不，她好像不怎麼愛說話，即使握著了，也不會回答的吧。

“太好了，暫時滿足了嗎？”

我重新握住刀柄，感到她恢復成了往常的輕盈。

我沒有使用黑化。但儘管如此，也能夠感受到這種輕盈，看來是因為我用我的血馴服了她吧。


現在，也是如臂使指般的感覺,不過，之前是靠黑化。現在是因為『絕怨鉈』接受了我，已經無需使用黑化了。不用靠黑魔力來包裹住她，而是獻出自己的血，讓她在真正的意義上。以更高的水平和我的身體同化、同調。


那麼，這樣能產生多大的效果呢……現在在這裡，還是不能試驗的。

“到時以阿瓦隆的亡靈騎士為對手，試試吧。”

我又多了一件樂事。嘛，對於喝不到鮮血的伙伴來說，她卻不太開心。

“那麼，接下來呢……是‘惡食’還是‘行世之死’呢”

我將吃飽的『絕怨鉈「首斷」』再次沉入陰影中，但我又對接下來該咋辦而感到苦惱呢？

我不知道我暈過去了多長時間。但是，西蒙還沒來叫我，所以沒有暈到五號。

雖然知道，但是想要像這樣繼續下去的話，也很困難。恐怕“極惡食”和“行世之死”都不是用一般手段就能滿足的吧。我不得不身要再做幾次，損傷身心，甚至會削減壽命的事情吧。

“呵，與其煩惱，不如按順序來吧。”

真要算的話，我得到的第二把詛咒武器，其實是『巴基瑞斯克的骨針』,不過，這傢伙在代達羅斯城牆上，與沙利葉的戰鬥中被毀掉了。所以，現在算我的第二號的話……

“怎麼了，出來啊，小柩”

我一呼喚，她就像是十分害羞地，從影子中露出了個漆黑手套的指尖。

如果是平常的話，她肯定會高興地回應我，但這樣的反應……果然，她還是很在意吧。

“不好意思，小柩。我也讓你擔心了”

我跪下膝蓋，把藏在影子中的手套抓了出來。

她大概是在抵抗吧，手套像魚一樣在手中蠕動著，但她也並不是真的不願吧。不然她就會認真的使用魔手，大鬧一番吧。

“你照顧著那時，像個空殻的我。多謝你了”

已經不會讓你再看到我那難看的樣子了。我會為了成為值得你驕傲的主人而努力的。

然後，我將雙手插進『黑鎖咒縛「鐵檻」』之中。

“嗚……主大人人，你已經不生氣了嗎？”

多虧戴上了手套，我才能聽到小柩的聲音。

“我沒有生氣。是我不好”

我的確是被強行遣返回了斯巴達，但如果小柩順著我的任性，帶我去追菲奧娜的話……即使以這種半吊子的覺悟去追，我也做不了任何事吧。不，根本就追不上。沒有任何意義，不會有任何成果，只會像傻瓜一樣摔倒在阿瓦隆的街道上而已。

“那麼，小柩可以繼續照顧主人嗎？”

回到屋子裡後，她和西蒙一起照料著我，可是小柩卻十分明顯地和我保持著距離。只在搬運我的時候才靠近我。然後就一直沒有露面地工作著。

一想到她以自身意志選擇了這麼做，我就覺得心如刀絞。

“啊，我需要你。往後餘生，能不能繼續把力量借給我？”

“主、主、主、主、主人大人啊ー！！”

哇啊。她發出這般巨大的叫聲，突然間，黑髮觸手迅速的從手套上迸發出來。

“哇！？”

我對其氣勢和數量，稍感害怕。我會就這樣被她纏住嗎？一瞬間，我綳緊了身體,不過，那數量龐大的觸手如波浪般涌過，在我眼前組成了一個大繭。


那是由黑髮編織出的巨大的繭。

到底是怎麼回事啊，正當我困擾的時候，就像蝴蝶孵化一般，漆黑的繭破裂開來。

“嗚哇，主人大人！”

從繭中飛出的小小人影，用尖銳的聲音大叫著。


沒錯，那的確是個人。

耀眼的雪膚，年幼少女的裸體。散發著孩子般純粹光芒的黑色瞳孔，還有那最引人注目的，細膩又艷麗的黑色長髮。


這樣的女孩子，一邊叫我主人大人，一邊抱住了我。

“小柩我，往後餘生也會一直、一直、一直陪在主人大人身邊的！”

“什麼！？小柩，你這，為什麼，變成人了……”

這緊緊地摟著我的腰，哭個不停的孩子，無論怎麼看，都是小柩。她的面龐，我在最後的玫瑰的夢中也看到過，最近她也利用暴君之鎧頭部的顯示器的功能，徒勞的播出了自己的臉，主張著自己的存在，所以我已經十分熟悉她的長相了。


雖說我沒認錯，她的確我知道的小柩的人形姿態……但她還是第一次像這樣，以活生生的身體出現在現實中。

“哈！？這、這是……小柩，不知不覺，就變成了真正的樣子了！？”

“不，你真正的樣子是個手套吧。”

看來，小柩自己也沒有注意到自己已經變成了活生生的肉體了。她是不知不覺間就變成這個樣子的。

從緊緊抱著我的小柩的身體裡，我感受到了血液流動的溫暖和她肌膚的柔軟。我輕輕用雙手觸摸她那華奢的肩膀，那只可能是人類的肉體的感觸也傳了過來。

“啊，小柩思念主人的心情，創造了奇跡呢！”

“……原來如此，原來是根據和史萊姆分身相同的原理塑造出身體的”

在像愛做夢的少女一樣大聲叫喊的小柩的身旁，我冷靜地看出了她的真面目。


傲慢杰姆能製造出就像是真人般的分身，而到了混沌熊兔，甚至能完全再現出與別人外表完全相同的精巧分身。多虧我得到了第六加護，所以能使用模擬水屬性，能自由自在地操縱與史萊姆相似的，液態的魔力物質。


既然能製作血液，當然也就能再現出它的擬態能力。

第六加護中最重要的是治癒能力，所以還沒有嘗試過這個能力……但我沒想到，竟然會因為小柩的意思製造出她的肉體。

“好厲害，好厲害啊，主人大人！有了這個身體，小柩就能為主人服務了，嘿嘿”

“喂，快住手，這下真的很糟糕！”

蹭蹭，小柩用她那艷麗的身體的摩擦著我的身體，我真的十分焦急。小柩的外表是黑髮美少女，年紀看起來比烏露絲拉還要年幼一點,但儘管如此，若是這樣被她裸體抱緊，並磨蹭的話，實在讓我受不住。雖然我不是蘿莉控,可能，也許，恐怕我不是蘿莉控的可能性十分的高，但我還是無法在被做這種事的情況下，都能保持冷靜啊。

“小柩，終於解禁了作為女僕的真正的服務！”

“等一下，你真的別再這麼做了，我求你了，你先離開我下吧——喂，你那頭髮別再亂動了！”

我的雙手雙腳被她那沙沙活動著的長長黑髮給緊緊綁住，我好不容易，總算讓將我綁成一字型的小柩放手了。可惡、小柩這傢伙，這麼快就能自如的使用人類的身體了。

“嗚，主人大人，太無趣了。”

誰會成為那上鈎的魚啊。她在各種意義上都太危險了。


但是，重新一看……小柩的身體，的確只會讓人認為是真正的人類少女。雖說裡面是史萊姆樣的魔力物質，但身體並不薄，也不透明，並不只是大致的模仿出了人型。她身上沒有一道斑紋，光滑的皮膚，指甲、肚臍、鎖骨……完美地再現出了人體。即使是她那只會說些怪話的嘴巴，其中也能窺見漂亮整齊的白牙和小巧的舌頭。那麼她身體的內部，又再現到了何種程度呢？


雖說我十分在意,不過，現在不是在意這種事的時候。話說回來，我不應該這麼認真的觀察小柩的裸體吧。

“總之，你能不能先恢復成原樣？”

“不要”

我作為主人卻被女僕給馬上拒絕了。

嘛，小柩似乎非常高興她能得到這個身體，所以不想這麼輕易地放手吧。

從魔力的氣息來看，這也不是能輕鬆變回變去的事。事到如今，我也發現我體內的魔力已經減少了許多了。


在構築小柩的肉體時，我消耗了相當多的魔力。就算讓她散去身體，變回原樣，消耗的魔力也不會全部返還回來吧。剛一出來，就消散掉的話，是挺可惜的。

“知道了，那你就先穿上衣服吧。或者說，你不能做出衣服嗎？”

“嗯，小柩也不知道。啊，但是用頭髮的話……”

小柩操控起蠢動的黑髮，讓髮梢纏繞到手腳上。然後，細長的頭髮編織成布狀，覆蓋住手臂和裸足的手套和襪子就這麼完成了。

“哼哼，怎麼樣啊！”

她就像是在展示一般，光明正大的仁王立著。

為什麼先做手套和襪子啊？作為女孩子，你有更應該要先遮住的部分吧。

“我知道了，夠了”

我毫不可惜地將視線從她暴露出的幼小的身軀上移開，脫下自己穿著的長袍，披在了小柩身上。

“哇哈！”

“總之，你先去上面隨便找點衣服穿吧。雖說可能會有點大，但如果是沙利葉的衣服，你應該也不是不能穿”

“恩，但我作為女僕長，很在意自己這麼擅自借用沙利醬的衣服啊。”

明明是詛咒，卻在意這種細節。雖說是擬態，但在她擁有人身的現在，她似乎已經是真正的人類了。

“一會兒會給小柩買女僕裝的，也會給沙利葉買衣服補償她的，別在意，快去穿吧。”

“好的”

她大概是認可了這事吧，小柩拖著長長的黑髮和我的大長袍，啪嗒啪嗒的跑出了地下室。

“唉……總感覺，好累啊”

感覺完全被意外事件給消耗了心神。


嘛，小柩非常高興，而我也在無意中找到了新魔法的可能性，所以結果還算是不錯吧……

“……難道說，其他傢伙不會也實體化吧”

接下來要多加注意了。


我提起警惕，繼續呼喚著下一把詛咒武器。

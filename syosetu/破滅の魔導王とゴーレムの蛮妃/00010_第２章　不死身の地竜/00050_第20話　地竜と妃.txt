從空中看到的古代地龍，像山一樣巨大的背面。

龍的巨大身軀在搖晃，緩緩地向這邊回頭。

我想強硬地轉換情緒。

盡力不去想在這個世界上初次相遇到好心的大叔的事情。

如果此時由於後悔而使判斷延遲的話，我恐怕只有死路一條。

沒錯，我是個無論何時都能快速轉換情緒的人。

沒有問題。我轉換情緒的――

哥雷已經開始落下來了。

這傢伙只是用驚人的腳力跳躍而已，並不是能飛上天空。

上升完後，當然就只會下落。

落下時，有一瞬間與古代地龍的目光相交了。

渾身都起雞皮疙瘩。

什麼感情都沒有窺見到，就只是漆黑的眼珠。

連殺氣都沒有感覺到。

這傢伙大概都沒有把我們的存在當做螞蟻以上的程度。

哥雷幾乎無聲地落地，把我輕輕地放在地面上。

看到我站起來的時候，哥雷的重心一下子落了下來――

就如雪白的箭一般，一口氣衝向地龍。

白色的拳頭像怒吼般。哥雷的第一擊，漂亮地擊中了。

至今為止，把任何敵人都能一擊必殺的攻擊，必殺的右直拳。

直擊地龍側腹的瞬間，那岩盤般鱗片的外皮像是巨大的火山口一樣瞬間凹陷。

――但是，古代龍沒有倒下。

哥雷就那樣開始了猛烈的擊打。

從左到右高速地釋放出暴風雨般的毆打，地龍的腹部被亂打一通。

每當哥雷帶有致死威力的沉重一擊灌入進地龍的外殼，也許應該稱作厚實的外皮，都凹陷成了大量火山口似的形狀。

但是，古代地龍卻毫無動搖。

等下，剛才的確感覺到破壞了內臟，但現在……。

看到對方沒有倒下後，哥雷就那樣沿著敵人的肩膀一瞬間就爬上了脖子處。

就在那裡哥雷像是陀螺一樣回轉，對準巨龍粗大的脖子，從上面像斷頭台要把延髓給割斷一樣。

哥雷在空中旋轉，用柔韌的雪白玉足，發出像鞭子一樣的嗚嗚聲。

響起了非常沉重的衝擊聲。

無論地龍有多巨大，這衝擊聲聽上去肯定是脖子被折斷的聲音。

我確實親眼看到這座山一樣的身軀一瞬間沉入數米。

但是，古代地龍沒有倒下。

不僅是身軀的尺寸，連釋放的存在感也大過頭，濃縮的土褐色幾乎與黑同樣的龍，連搖晃都沒有，僅僅是站立在那裡。

那傢伙的眼睛還是沒有包含任何感情，一點痛苦的表情都沒有浮現出來。

完全沒有效果嗎？即使是剛剛那樣猛烈的攻擊！？

這傢伙既然是由蛋白質和鈣構成，那麼即使承受那樣的攻擊，卻連細胞都破壞不了，這太奇怪了，怎麼可能有這種事。

「無論哥雷有多麼強也殺不死古代龍”腦海中浮現出這句話來。

這傢伙，難道說用物理攻擊是沒有效果的嗎……？

目瞪口呆，茫然地抬頭看著地龍的我。

突然，像是被什麼看不見的墻彈開了一樣，哥雷被甩到空中。

怎麼了，到底發生了什麼？

地龍朝著哥雷的方向望去。

大大張開的口中，可以看見土的粒子凝聚在一起。

緊接著，宛如石柱一般巨大的岩石，朝著哥雷發射出去了。

剛才將我們站著的地面給剜去的就是這招！

糟糕。

哥雷現在在空中，而且她完全保持不了平衡。

面對放射出去的巨岩，我慌忙地舉起右臂。

「――〈NTR〉！！！」

這是我至今為止對猴子使用好幾次的技能。

說實話，如果是猴子的石彈的話，我可是有信心將它們同時發射的幾十萬發石彈給彈回去。

〈NTR〉就是如此強力的技能。

然後，與在聖堂支配格雷姆的時候不同，現在，有切實使用技能抓住的感覺。也就是說，〈NTR〉對於這個石彈，毫無疑問是有效果的。我打算奪取其控制權來彈回去。

地龍放出的石彈，和猴子相比確實是很巨大。

儘管如此，那最多隻是直徑近兩米，深度也就數米左右的程度而已。

再加上，數量只有一個。

作為術的規模和質量，按理來說應該是毫無問題一瞬間就能處於控制範圍的東西。

――但是，古代地龍的石彈卻沒有停止。

也就若干減弱速度的程度，還是那樣朝哥雷的方向直行。

「什……！？」

啞口無言的我

雖說是這樣，但哥雷應對得很迅速。

哥雷在石彈延緩的一瞬間，重整姿勢，踩著地面向著逼近的石彈舉起右臂。

哥雷的必殺右直拳與古代地龍吐出的巨岩正面產生衝突。

巨大力量直接的碰撞，使周圍產生了衝擊波。

面對強烈的風壓，我不由得皺起了眉頭。

石彈一邊將地面剜得亂七八糟，一邊將伸出拳頭的哥雷推向後方。

哥雷和石彈通過的痕跡散佈大量土砂和瘴氣，被破壞的地表形成一條溝。

那個哥雷在力量上竟然輸了。

恐怕是質量差距太大了。

但是這時，地龍的石彈漸漸地從中心開始產生裂縫。

裂縫擴散到整個巨石――

緊接著，石彈像是爆開一樣破碎散落。

哥雷被破碎的衝擊捲入，被推到後方數米。

但是，還好沒事。她緊緊地踩著在大地上。

「太好了……。總算是撐過了……！」

我不由得鬆了一口氣。

但是，在那時，我注意到了異變。

在哥雷右臂的根部，有一道橫著的裂縫。

不久，那個裂縫沿著哥雷的纖細右臂裂開了一圈……。

――她的胳膊從根部斷裂，啪一聲地掉在了地上。

哥雷就那樣搖搖晃晃地失去了平衡，仰面倒下了。

她拚命地想站起來，但是身體僵硬而動彈不得。

哥雷怎麼了。

我臉上失去了血色。

強硬用不聽使喚的腳全力衝刺，慌忙地跑到她的身旁。

跪在地上，抱起躺著在地上的哥雷的身體。

哥雷身體的表面上，像是纏繞著泥土瘴氣的殘渣一樣的東西，看上去有種奇妙的不協調感。

難道說，被那傢伙的魔力或者瘴氣給纏上了？

在這種狀態下，如果地龍發射第二彈的話，毫無疑問我肯定會被捲入而死，不能說這樣的話。但是放任不管的話，我家的哥雷也會被破壞。

幸好地龍在沉默著。

這麼說來，這傢伙，在我們從高空中無防備地落下時也沒有追擊。

那個石彈，難道說連射不了……？

不管怎樣，我先趕緊抱住哥雷的肩膀。

臉微微地移動，赤紅色的瞳孔在凝視著我。

太好了，哥雷還有意識。

平時，我接觸到的部分身體，這傢伙會固執地使其變得柔軟、溫暖，但現在她的身體卻像真正的雕刻一樣，變得如此冰冷僵硬。

糟糕。我可沒有聽過格雷姆應急處置的講座誒。

當我走投無路的時候，地龍的嘴裡，土的粒子終於再次開始聚集起來。

第二發要來了

從一開始就不存在拋棄無法動彈的哥雷來迴避攻擊的選項。

我只會停留在這個地方，只能由我來防住。

我左手抱著哥雷，右手舉起向著地龍。

「可惡！――〈NTR〉！！！」

可以看見逼近的巨岩的尖端，稍微變黑了。

拜託了，見效吧。

給我轉彎啊！

一點點……雖然只是一點點，但確實偏離了軌道。

驚人的風壓和轟鳴聲穿過我們的身旁。

看到風壓穿過的我們的右側，地面被破壞得亂七八糟。

在那裡，形成了散發出瘴氣的漆黑巨大的溝。

「得，得救了嗎……。」

但是，為什麼……？

我的腦袋又開始高速地回轉。

為什麼這次的軌道偏離了？

在軌道偏離之前，能看到石彈的一半已經變色了。

與〈NTR〉幾乎不起作用的最初的第一發有什麼不同？

回想起來，對最初的一發使用〈NTR〉時是在命中哥雷之前。

但是，對於能預測到的第二發，在地龍的石彈生成的同時也開始發動〈NTR〉。

――是時間啊。

恐怕是因為時間不足。

不知是魔力還是瘴氣，由於地龍的石彈帶有難以置信的濃密而又龐大的力量，平時能一瞬間就完成的支配，而現在需要大量的時間。

但是，在此之上更早地發動已經是不可能的了……。

也就是說，使其偏離彈道已經是極限了嗎？

既然不能置於完全的控制下，就無法使用〈NTR〉來反擊。

再加上，干涉這個地龍的石彈，會吞食龐大的魔力。

想要彈開幾十發什麼的，怎麼都覺得不可能。

即使一直防禦，到最後我也會因為燃料不足，在那一瞬間敗北。

雖說如此，也不可能抱著無法動彈的哥雷以我的腳力來逃脫。

抱著……嗎。話說哥雷的體重大概是多少？

這傢伙平時有著令人驚訝的身體操作能力，完全沒有讓我意識到體重什麼的。

不，倒不如說平時從哥雷身上感覺到過輕的體重，不知為何，簡直就像是在意中人的面前謊稱體重，害羞的少女一樣，有著這樣的味道。

雖然現在抱在懷裡能感覺到沉甸甸的重量，但這種事情本身還是第一次經歷。

不管怎麼說，既然這傢伙受傷了，那麼帶她逃跑是不可能的。

以我的性格來說，丟下這傢伙逃跑是更加不可能的。

……事到如今，不管怎樣，把握地龍發射石彈的間隙，在這之中由我來向它發動攻擊，想辦法找出一條活路來。

我想起了在『魔術入門Ⅳ』中讀到的幾個入門魔術。

因為害怕會暴走，所以至今為止都沒有實際使用過，但是在這個危機的狀況下，擔心安全性而猶豫使用的就是白癡。

而且，我應該是能使用“魔導”的。

與不斷生成的普通的土屬性“魔術”別具一格。

應該biubiu地像用無線電操作一樣。像猴子一樣。

我下定決心了。

把坐標定在腳下的地面生成，集中意識。開始詠唱。

「――〈土之大槍〉。」

在地表上聚集著土的粒子。

像纏繞在棒狀上的粒子們逐漸聚集成形，長度2米左右的褐色粗壯的槍被生成了。

好。做出來了。

成功了。和教科書一樣。

真厲害。我也能做出來了。

入門書雖然也有生成斧或錘的魔術，不過，在這種場合我應該把發射作為前提，所以投擲武器的槍應該是最佳選擇。如果以跟石彈同樣的要領發射，恐怕也是形狀上最有貫穿力的武器。

……問題就是從這裡開始的吧。

魔導，基本上應該是怎樣用的？

恐怕在原理上，跟平時使用的〈NTR〉應該沒有太大的差別。是操作敵人的魔術，還是操作自己生成的魔術，就只是對象的不同。

而且那些猴子們只是在普通地使用。

猴子能做到的，身為萬物之靈的我不可能做不到。

我絕對不會輸給那群猴子的。

姑且，用跟〈NTR〉同樣的要領，試著對槍禮貌、認真地請求。

――槍先生，我想打倒那隻大蜥蜴，務必想請求您的幫助……。（這句翻得我好尷尬）

緊接著，眼看著槍開始變得漆黑。

染上不詳漆黑色的槍，在空中漂浮著。

那樣的威容，簡直就像，魔王的地獄之槍。

噢，做到了！

看到了嗎，猴子們！我終於能跟你們並列了！

話說，如果是操作自己生成的魔術的話，不像在使用〈NTR〉的時候，即使不發出命令也能移動。

試著輕輕地移動一下。

槍浮在我手邊，咕嚕咕嚕地回轉。

嗯，很合適。感覺不錯。

我把槍瞄準了地龍的額頭。

「槍啊，貫穿吧！！！」

〈土之大槍〉像導彈一樣飛出去了。

對著地龍的額頭，筆直地突進。

音速的槍的一擊，看上去就能夠打穿龍的眉間。

――但是，槍在擊中地龍額頭之前，被神秘的防護罩給阻止了。

啊！？那是什麼！？

我可沒有事先聽過說明，這樣的東西！

想要穿透的大槍和被擋住又看不見的障礙。

大槍以猛烈的氣勢Gyurugyuru（擬聲詞）地像鑽頭一樣持續旋轉著，對方的障壁也不動搖地持續推回原位。兩者展開了激烈的爭鬥。

我注意到了。

難道說就是這個嗎。最初把哥雷擊飛到空中，看不見的墻壁的真面目。

激烈的對撞正在上演，漆黑的大槍和透明的障壁。

……但是，我的槍不久就用盡，變成黑色粒子崩潰了。（難得男主表現一回，結果這麼慘淡）

怎、怎麼會，槍先生。

我第一次的普通必殺技……。初次登場就這麼簡單地輸了……。

巨大的地龍，悠然地站在那裡。

那瞳孔裡沒有感情的色彩。

望著如喪服黑色般的瞳孔，我茫然若失。

哥雷的最強物理攻擊無效。

愛用的〈NTR〉幾乎被無視，憑力量衝撞過來。

然後，好不容易覺醒了的魔導攻擊，竟然被神秘的防護罩簡單地給無效化了。

土屬性的魔導是伴隨質量的，本來對魔術性的防禦是很有攻擊性的，卻像理所當然一樣地給彈開了。

這傢伙，完全是犯規了吧……。